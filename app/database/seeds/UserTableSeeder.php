<?php
 
class UserTableSeeder extends Seeder {
 
    public function run()
    {
        DB::table('users')->delete();
 
        User::create(array(
            'username' => 'rama',
            'password' => Hash::make('rama')
        ));
 
        User::create(array(
            'username' => 'rama2',
            'password' => Hash::make('rama2')
        ));
    }
 
}