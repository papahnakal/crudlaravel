<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
/*	Route::get('login', array('as' => 'login', function () {
    	return View::make('nerds.login');
		}))->before('guest');
	Route::post('login', function () {
        $user = array(
            'username' => Input::get('username'),
            'password' => Input::get('password')
        );
        
        if (Auth::attempt($user)) {
            return View::make('nerds.index')
                ->with('flash_notice', 'You are successfully logged in.');
        }
        
        // authentication failure! lets go back to the login page
        return Redirect::route('login')
            ->with('flash_error', 'Your username/password combination was incorrect.')
            ->withInput();
});*/
	//Route::get('nerds/index');
	Route::resource('nerds', 'NerdController');	
	/*Route::group(array('prefix' => 'nonuser', 'before' => 'guest'), function()
		{
    		Route::resource('nerds', 'NerdController');	
    		//Route::resource('guest', 'APIController');
    		//Route::resource('nerdlv','NerdlvController');
	//return View::make('hello');
		});*/
	Route::group(array('prefix' => 'api/v1', 'before' => 'auth'), function()
		{
    		Route::resource('guest', 'APIController');
    		Route::resource('nerdlv','NerdlvController');
	//return View::make('hello');
		});
	/*Route::get('nerds/login', array('before' => 'auth.basic', function()
{
    return View::make('hello');
}));*/
	Route::get('profile', array('as' => 'home', function () {
		$nerds = Nerd::all();
    return View::make('nerds.profile')->with('nerds', $nerds);;
	}));
	Route::get('login',array('as'=>'login','uses'=>'LoginController@showLogin'));
	Route::post('login',array('as'=>'LakukanLogin','uses'=>'LoginController@doLogin'));
	
	Route::get('logout', array('as' => 'logout', function () {
    	Auth::logout();

	    return Redirect::route('nerds.index')
        ->with('flash_notice', 'You are successfully logged out.');
	}))->before('auth');

	
		
