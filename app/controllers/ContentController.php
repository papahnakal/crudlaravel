<?php

class ContentController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
		$content=Content::all();

       //load the view and pass the nerds
        return View::make('nerds.contents')
            ->with('nerds', $content);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return View::make('nerds.createcontent');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		     $rules = array(
            'judul'       => 'required',
            'isi'      => 'required',
            
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('nerds/createcontent')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $content = new Content;
            $content->judul       = Input::get('judul');
            $content->isi      = Input::get('isi');
            $content->save();

            // redirect
            Session::flash('message', 'Successfully created nerd!');
            return Redirect::to('nerds/content');
	}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
