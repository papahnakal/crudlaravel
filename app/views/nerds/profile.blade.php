<!DOCTYPE html>
<html>
<head>
    <title>Look! I'm CRUDding</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('profile') }}">Nerd Alert</a>
    </div>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('profile') }}">View All Nerds</a></li>
         @if(Auth::check())
        <li><a href="{{ URL::to('nerds/create') }}">Create a Nerd</a></li> 
               
                    <li><a href="{{ route('logout') }}">({{Auth::user()->username}})Logout</a></li>
                @else
                    <li><a href="{{ route('login') }}">login</a></li>
                @endif
    </ul>
</nav>
<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
 @if(Session::has('flash_notice'))
            <div class="alert alert-info">{{ Session::get('flash_notice') }}</div>
        @endif
<h2>Welcome "{{ Auth::user()->username }}" to the protected page!</h2>
<p>Your user ID is: {{ Auth::user()->id }}</p>

<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <td>ID</td>
            <td>Name</td>
            <td>Email</td>
            <td>Nerd Level</td>
            <td>Actions</td>
        </tr>
    </thead>
    <tbody>
   <!--  {{$nerds = Nerd::all();}} -->
    @foreach($nerds as $key => $value)
        <tr>
            <td>{{ $value->id }}</td>
            <td>{{ $value->name }}</td>
            <td>{{ $value->email }}</td>
            <td>{{ $value->nerd_level }}</td>

            <!-- we will also add show, edit, and delete buttons -->
            <td>

                <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                <!-- we will add this later since its a little more complicated than the other two buttons -->
                 {{ Form::open(array('url' => 'nerds/' . $value->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete this Nerd', array('class' => 'btn btn-warning')) }}
                {{ Form::close() }}
                <a class="btn btn-small btn-default" href="{{ URL::to('api/v1/guest/' . $value->id) }}">Show API</a>
                <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                <a class="btn btn-small btn-success" href="{{ URL::to('nerds/' . $value->id) }}">Show this Nerd</a>

                <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                <a class="btn btn-small btn-info" href="{{ URL::to('nerds/' . $value->id . '/edit') }}">Edit this Nerd</a>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>
  <a class="btn btn-small btn-default" href="{{ URL::to('api/v1/guest') }}">Show All API data</a>
  <a class="btn btn-small btn-warning" href="{{ URL::to('api/v1/nerdlv/'.$value=1) }}">Show API who is nerd lv 1</a>
  <a class="btn btn-small btn-info" href="{{ URL::to('api/v1/nerdlv/'.$value=2) }}">Show API who is nerd lv 2</a>
   <a class="btn btn-small btn-success" href="{{ URL::to('api/v1/nerdlv/'.$value=3) }}">Show API who is nerd lv 3</a>


</div>
</body>
</html>