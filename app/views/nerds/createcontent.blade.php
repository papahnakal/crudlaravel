<!DOCTYPE html>
<html>
<head>
    <title>Look! I'm CRUDding</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('content') }}">Nerd Alert</a>
    </div>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('nerds') }}">View All Nerds</a></li>
        <li><a href="{{ URL::to('nerds/create') }}">Create a Nerd</a>
		<li><a href="{{ URL::to('content/create') }}">Create a Content</a>
    </ul>
</nav>

<h1>Create a Content</h1>
{{ HTML::ul($errors->all()) }}

{{ Form::open(array('url' => 'content')) }}

    <div class="form-group">
        {{ Form::label('judul', 'Judul') }}
        {{ Form::text('judl', Input::old('judul'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('isi', 'Isi') }}
        {{ Form::text('isi', Input::old('isi'), array('class' => 'form-control')) }}
    </div>

   

    {{ Form::submit('Create Content!', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

</div>
</body>
</html>