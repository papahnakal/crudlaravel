<html>
<head>
    <title>Look! I'm CRUDding</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
    @if(Auth::check())
    <a class="navbar-brand" href="{{ URL::to('profile') }}">Nerd Alert</a>
    @else
    <a class="navbar-brand" href="{{ URL::to('nerds') }}">Nerd Alert</a>
    @endif
        
    </div>
    <ul class="nav navbar-nav">
    @if(Auth::check())
    <li><a href="{{ URL::to('profile') }}">View All Nerds</a></li>
    @else
    <li><a href="{{ URL::to('nerds') }}">View All Nerds</a></li>
    @endif
      
         @if(Auth::check())
       <li><a href="{{ URL::to('nerds/create') }}">Create a Nerd</a></li>
               
                    <li><a href="{{ route('logout') }}">({{Auth::user()->username}})Logout</a></li>
                @else
                    <li><a href="{{ route('login') }}">login</a></li>
                @endif
    </ul>
</nav>

<h1>Showing {{ $nerd->name }}</h1>

    <div class="jumbotron text-center">
        <h2>{{ $nerd->name }}</h2>
        <p>
            <strong>Email:</strong> {{ $nerd->email }}<br>
            <strong>Level:</strong> {{ $nerd->nerd_level }}
        </p>
    </div>

</div>
</body>
</html>