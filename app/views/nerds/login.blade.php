<!DOCTYPE html>
<html>
<head>
    <title>Look! I'm CRUDding</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('nerds') }}">Nerd Alert</a>
    </div>
    <ul class="nav navbar-nav">
         <li><a href="{{ URL::to('nerds') }}">View All Nerds</a></li>
         @if(Auth::check())
        <li><a href="{{ URL::to('nerds/create') }}">Create a Nerd</a> </li>
               
                    <li><a href="{{ route('logout') }}">({{Auth::user()->username}})Logout</a></li>
                @else
                    <li><a href="{{ route('login') }}">login</a></li>
                @endif
    </ul>
</nav>

<h1>All the Nerds</h1>

<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
@if(Session::has('pesan'))
            <p class="alert alert-warning">{{ Session::get('pesan') }}</p>
        @endif
 @if (Session::has('flash_error'))
        <div id="flash_error">{{ Session::get('flash_error') }}</div>
    @endif
        <!-- Jika tabel biodata memiliki isi, tampilkan isi berikut -->
        
        <!-- Siapkan tombol untuk membuat biodata baru -->
{{ Form::open(array('route' => 'login')) }}
<div class="panel panel-primary">
  <!-- Default panel contents -->
  <div class="panel-heading"><h3>Login</h3></div>
        <!-- if there are login errors, show them here -->
        <table class="table">

        <tr>
            <td>{{ Form::label('username', 'Username') }}</td>
            <td>{{ Form::text('username', Input::old('username'), array('placeholder' => 'your username')) }}</td>
            <td>{{ $errors->first('email') }}</td>
        </tr>

        <tr>
            <td>{{ Form::label('password', 'Password') }}</td>
            <td>{{ Form::password('password') }}</td>
            <td>{{ $errors->first('password') }}</td>
        </tr>

        <tr><td></td><td>{{ Form::submit('Login') }}</td><td></td></tr>
        
        </table>
</div>
    {{ Form::close() }}
</div>
</body>
</html>